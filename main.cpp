#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include "tree.h"
#include<algorithm>
using namespace std;

int main()
{
	Tree<string> mytree;

	ifstream inFile("test.txt");
	string content;
	string temp;
	string indexStr;
	int index = 0;
	while(getline(inFile,temp)){
		istringstream tmp(temp);
		index++;
		while(tmp >> content){
		//	cout<<content << " " << index <<endl;
			indexStr = to_string(index);
			content = content + "	"+ indexStr;
			mytree.insert(content);
			//cout;
		}
	}
	mytree.inorder();
	return 0;
}